<?php

class EventWidget extends WP_Widget {

	public function __construct() {
		parent::__construct('events_widget', 'Events widget',
			array('description' => 'Event widget'));
	}

	public function widget( $args, $instance ) {
		echo do_shortcode("[eventshortcode status='" . $instance['event_status'] . "' number='" .$instance['number'] . "']");
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['event_status'] = $new_instance['event_status'] ;
		$instance['number'] = (int) $new_instance['number'];
		return $instance;
	}

	public function form($instance) {
		$instance['event_status'] = !empty($instance['event_status']) ? $instance['event_status'] : 'open';
		$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 2;
        ?>
        <div>
            <h4>Status</h4>
            <p>
                <input type="radio" name="<?php echo $this->get_field_name( 'event_status' ); ?>"
                       value="open" id="<?php echo $this->get_field_id( 'event_status' ); ?>" <?php checked( $instance['event_status'], 'open'); ?>>
                <label for="<?php echo $this->get_field_id( 'event_status' ); ?>">OPEN</label>
            </p>
            <p>
                <input type="radio" name="<?php echo $this->get_field_name( 'event_status' ); ?>" value="invite" id="event-invite" <?php checked( $instance['event_status'], 'invite' ); ?>>
                <label for="event-invite">BY INIVITATION</label>
            </p>
        </div>
        <div>
            <p>
                <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of events to show:' ); ?></label>
                <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" step="1" min="1" value="<?php echo $number; ?>" size="3" />
            </p>
        </div>
		<?php
	}

}

add_action("widgets_init", function () {
	register_widget("EventWidget");
});
