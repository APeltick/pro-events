<?php

add_shortcode('eventshortcode', 'event_shortcode_function');

function event_shortcode_function ( $atts ) {
	extract(shortcode_atts(array(
		"number" => 2,
		"status" => 'invite'
	), $atts));

	global $post;
	$postslist = get_posts(
		array( 'post_type' => 'events',
		       'posts_per_page' => $number,
		       'meta_query' => array(
		           array(
		           	   'key' => 'event_status',
			           'value' => $status
		           ),
			       array(
			       	   'key' => 'event_date',
			       	   'value' => date('Y-m-d'),
				       'compare' => '>='
			       )
		       ),
		       'order'=> 'DESC') );

	ob_start();
	?>
	<h3>Events</h3>
	<ul>
		<?php
		foreach($postslist as $post){ setup_postdata($post);
			?>
			<li>
				<a href="<?=the_permalink()?>">
					<?=the_title()?> (<?=$post->event_date;?>)
				</a>
			</li>
		<?php } ?>
	</ul>
	<?php

	$output_string = ob_get_contents();
	ob_end_clean();
	wp_reset_postdata();
	return $output_string;
}
