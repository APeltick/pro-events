<?php


add_action('add_meta_boxes', 'event_add_status_metabox');

add_action('add_meta_boxes', 'event_add_date_metabox');

function event_add_status_metabox() {

	add_meta_box(
		'event_status_id',
		'Event status',
		'event_status_callback',
		'events',
		'side',
		'high'
	);

}

function event_add_date_metabox() {

	add_meta_box(
		'event_date_id',
		'Event date',
		'event_date_callback',
		'events',
		'side',
		'high'
	);

}

function event_status_callback( $post ) {

	wp_nonce_field( basename( __FILE__ ), 'events_nonce');
	$events_meta = get_post_meta($post->ID);
	if ( ! empty ( $events_meta['event_status'] ) ) {
		if ($events_meta['event_status'][0] == 'open') {
			$open = 'checked';
		} else {
			$invite = 'checked';
		}
	}
	?>

	<div>
		<div>
			<input type="radio" name="event_status" value="open" id="event-open" <?=$open?>>
			<label for="event-open">OPEN</label>
		</div>
		<div>
			<input type="radio" name="event_status" value="invite" id="event-invite" <?=$invite?>>
			<label for="event-invite">BY INIVITATION</label>
		</div>
	</div>

	<?php
}

function event_date_callback( $post ){
	wp_nonce_field( basename( __FILE__ ), 'events_nonce');
	$events_meta = get_post_meta($post->ID);

	if ( ! empty ( $events_meta['event_date'] ) ) {
		$date = $events_meta['event_date'][0];
	}
	?>

	<div>
		<div>
			<label for="event-date">PICK EVENT DATE</label>
			<input type="date" name="event_date" value="<?=$date?>" id="event-date">
		</div>
	</div>

	<?php
}

function event_meta_save( $post_id ) {

	$is_autosave = wp_is_post_autosave( $post_id );
	$is_revision = wp_is_post_revision( $post_id );
	$is_valid_nonce = ( isset( $_POST[ 'events_nonce' ] ) && wp_verify_nonce( $_POST[ 'events_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';

	// Exits script depending on save status
	if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
		return;
	}

	if (isset( $_POST['event_status'] ) ) {
		update_post_meta( $post_id, 'event_status', $_POST['event_status']);
	}

	if (isset( $_POST['event_date'] ) ) {
		update_post_meta( $post_id, 'event_date', $_POST['event_date']);
	}
}
add_action('save_post', 'event_meta_save');
