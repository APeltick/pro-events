<?php

add_action('init', 'register_post_type_events');

add_action('init', 'register_taxonomy_events');

function register_post_type_events(){
	register_post_type('events', array(
		'label'  => null,
		'labels' => array(
			'name'               => 'Event',
			'singular_name'      => 'Events',
			'add_new'            => 'Add new event',
			'add_new_item'       => 'Add new event item',
			'edit_item'          => 'Edit event',
			'new_item'           => 'New event',
			'view_item'          => 'View event',
			'search_items'       => 'Search events',
			'not_found'          => 'Events not found',
			'not_found_in_trash' => 'Events not found in trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Events',
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => null,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_rest'        => null,
		'rest_base'           => null,
		'menu_position'       => 5,
		'menu_icon'           => null,
		'hierarchical'        => false,
		'supports'            => array('title','editor','thumbnail'),
		'taxonomies'          => array('events_tax'),
		'has_archive'         => true,
		'rewrite'             => true,
		'query_var'           => true,
	) );
}

function register_taxonomy_events() {

	register_taxonomy('events_tax', 'events',  array(
		'label'                 => '',
		'labels'                => array(
			'name'              => 'Events Taxs',
			'singular_name'     => 'Event Tax',
			'search_items'      => 'Search Events Taxs',
			'all_items'         => 'All Events Taxs',
			'parent_item'       => null,
			'parent_item_colon' => null,
			'edit_item'         => 'Edit Event Tax',
			'update_item'       => 'Update Event Tax',
			'add_new_item'      => 'Add New Event Tax',
			'new_item_name'     => 'New Event Tax Name',
			'menu_name'         => 'Event Tax',
		),
		'description'           => '',
		'public'                => true,
		'publicly_queryable'    => null,
		'show_in_nav_menus'     => true,
		'show_ui'               => true,
		'show_tagcloud'         => true,
		'show_in_rest'          => null,
		'rest_base'             => null,
		'hierarchical'          => false,
		'update_count_callback' => '',
		'rewrite'               => true,
		'capabilities'          => array(),
		'meta_box_cb'           => null,
		'show_admin_column'     => false,
		'_builtin'              => false,
		'show_in_quick_edit'    => null,
	) );
}
