<?php
/*
Plugin Name: pro-events
Plugin URI: http://test.com
Description: event widget
Version: 0.0.1
Author: Alex
Author URI: http://test.com
*/

if ( ! defined('ABSPATH')) {
	exit;
}


require_once ('pro-events-cpt.php');
require_once ('pro-events-fields.php');
require_once ('pro-events-shortcode.php');
require_once ('pro-events-widget.php');


